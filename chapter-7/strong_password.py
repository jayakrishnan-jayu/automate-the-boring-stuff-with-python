import re

def is_strong(passwd):
    if len(passwd) < 8:
        return False

    for regex in ("[a-z]", "[A-Z]", "[0-9]"):
        if (re.compile(regex).search(passwd) == None):
            return False

    return True
    
    
print(is_strong("hello wordl5d"))