import re

def strip(text, char=None):

    if char:
        return re.sub(re.compile(char), "", text)

    left = re.compile(r"^\s*")
    right = re.compile(r"\s*$")

    text = re.sub(left, "", text)
    text = re.sub(right, "", text)

    return text


print(strip("  heyy 123  "))