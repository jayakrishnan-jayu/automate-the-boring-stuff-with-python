import re

def is_leap_year(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year & 400 == 0:
                return True
            return False
        return True

def is_date(date):
    regex = re.compile(r"\d\d/\d\d/\d\d\d\d")
    
    day, month, year = [int(x) for x in regex.search(date).group().split("/")]
    
    foo = (4, 6, 9, 11)

    if (month in foo and day <= 30) or (month not in foo and day <= 31):

        if (month == 2):
            if (is_leap_year(year)):
                return day <= 29
            else:
                return day<=28

        return True

    return False        

print(is_date("29/02/2020"))