### 1. What is the function that creates Regex objects?

```compille()```

---

### 2. Why are raw strings often used when creating Regex objects?

* so that \ characters won't be interpreted as escape characters

---

### 3. What does the search() method return?

* search method will return a match obect.

---

### 4. How do you get the actual strings that match the pattern from a Match object?

* By using the group() method.

---

### 5. In the regex created from r'(\d\d\d)-(\d\d\d-\d\d\d\d)', what does group 0 cover? Group 1? Group 2?

* Group 0 : Entire matched string
* Group 1 : The matched string that has the pattern in between the first parantheses
* Group 2 : The matched string that has the pattern in between the second parantheses

---

### 6. Parentheses and periods have specific meanings in regular expression syntax. How would you specify that you want a regex to match actual parentheses and period characters?

*using backslash \ character before the parantheses or period

---

### 7. The findall() method returns a list of strings or a list of tuples of strings. What makes it return one or the other?

* ```findall()``` will return a list if there is no groups(pattern inside`()`) otherwise a list of tuple

---

### 8. What does the | character signify in regular expressions?

* it signfies a or expression.

---

### 9. What two things does the ? character signify in regular expressions?

* It is used to specify optional pattern or to specify non greedy approach.

---

### 10. What is the difference between the + and * characters in regular expressions?

* ```*``` : zero or many of the pattern
* ```+``` : one or many of the pattern

---

### 11. What is the difference between {3} and {3,5} in regular expressions?

* ```{3}`` will search for exact three times of the preceding pattern group.
* ```{3,5}`` will search for  three to five times of the preceding pattern group.

---

### 12. What do the \d, \w, and \s shorthand character classes signify in regular expressions?

* ```\d``` will search for a digit.
* ```\w``` will search for word.
* ```\s``` will search for space character.

---

### 13. What do the \D, \W, and \S shorthand character classes signify in regular expressions?

* ```\D``` will search for a character that is not a digit.
* ```\W``` will search a character that is not a for word.
* ```\S``` will search for a character that is not a space character.


---

### 14. What is the difference between .* and .*? ?

* ```.*``` will search for any pattern.
* ```.*?``` will search for any pattern in a non greedy way.

---

### 15. What is the character class syntax to match all numbers and lowercase letters?

```[a-z0-9]```

---

### 16. How do you make a regular expression case-insensitive?

pass re.I as a second argument to compile method.

---

### 17. What does the . character normally match? What does it match if re.DOTALL is passed as the second argument to re.compile()?

* ```.``` is a wildcard. It matches everything exception newline character.
* when ```re.DOTALL``` is passed newline characters are also included.

---

### 18. If numRegex = re.compile(r'\d+'), what will numRegex.sub('X', '12 drummers, 11 pipers, five rings, 3 hens') return?

* 'x drumers, x pipers, five rings, x hens'

---


### 19. What does passing re.VERBOSE as the second argument to re.compile() allow you to do?

* It will allow re.compiler() to have a multiline pattern argument by ignoring whitespace characters in between pattern expression string.

---

### 20. How would you write a regex that matches a number with commas for every three digits? It must match the following:
```
'42'
'1,234'
'6,368,745'
but not the following:

'12,34,567' (which has only two digits between the commas)
'1234' (which lacks commas)
```
```
r"^d{1,3}(,\d}{e}*$)"
```
---

### 21. How would you write a regex that matches the full name of someone whose last name is Watanabe? You can assume that the first name that comes before it will always be one word that begins with a capital letter. The regex must match the following:
```
'Haruto Watanabe'
'Alice Watanabe'
'RoboCop Watanabe'
but not the following:

'haruto Watanabe' (where the first name is not capitalized)
'Mr. Watanabe' (where the preceding word has a nonletter character)
'Watanabe' (which has no first name)
'Haruto watanabe' (where Watanabe is not capitalized)
```
```
r"[A-Z][a-z]*\sWatanabe"
```
---

### 22. How would you write a regex that matches a sentence where the first word is either Alice, Bob, or Carol; the second word is either eats, pets, or throws; the third word is apples, cats, or baseballs; and the sentence ends with a period? This regex should be case-insensitive. It must match the following:
```
'Alice eats apples.'
'Bob pets cats.'
'Carol throws baseballs.'
'Alice throws Apples.'
'BOB EATS CATS.'
but not the following:

'RoboCop eats apples.'
'ALICE THROWS FOOTBALLS.'
'Carol eats 7 cats.'
```
```
r"(Alice|Bob|Carol)\s(eats|pets|throws)\s(apples|cats|baseballs)"
```