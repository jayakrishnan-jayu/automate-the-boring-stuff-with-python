
### 1. Which of the following are operators, and which are values?
|Operators|Values|
|----|-----|
|*|'hello'|
|-|-88|
|/|5|
|+||
||

---
### 2. Which of the following is a variable, and which is a string?
* spam  -  variable 
* 'spam' - string

---

### 3. Name three data types.
integer, string, float

---

### 4. What is an expression made up of? What do all expressions do?
* Expression is made up of values and operators.
* Expressions evaluates to a value.

---

### 5. This chapter introduced assignment statements, like spam = 10. What is the difference between an expression and a statement?

* Statements are actions that returns something, assignment statement assigns the value of right hand expression to left hand variable name.
* Expression are a combination of both values and operators which evaluates to a single value.

---

### 6. What does the variable bacon contain after the following code runs?

```python
>>> bacon = 20 # Value of variable bacon will be 20.
>>> bacon + 1 # This line will output 21 but the value of bacon variable will be the same.
```
---

### 7. What should the following two expressions evaluate to?

```python
>>> 'spam' + 'spamspam' # The two strings will be concatenated to 'spamspamspam'
>>> 'spam' * 3 # '*' becomes string replication operator and the value of string will be repeated 3 times. The output of the string will be 'spamspamspam'
```

---

### 8. Why is eggs a valid variable name while 100 is invalid?

A valid variable name will have
* No spaces between letters
* Doesn't begin with a number
* Will only contain numbers, letters and underscore

That is why eggs is a valid variable while 100 is invalid.

---

### 9. What three functions can be used to get the integer, floating-point number, or string version of a value?

```python
>>> str(value) # String version of a value
>>> int(value) # Integer version of a value
>>> float(value) # Float version of a value
```

---

### 10. Why does this expression cause an error? How can you fix it?


```python
>>> 'I have eaten ' + 99 + ' burritos.' # This line wil throw an error because '+' operator can only add two numbers or concatenate two strings. 
>>> 'I have eaten ' + str(99) + ' burritos.' # This can be fixed if 99 is converted into string.
```
