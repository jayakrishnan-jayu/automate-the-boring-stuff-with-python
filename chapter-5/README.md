### 1. What does the code for an empty dictionary look like?

```{}```

---

### 2. What does a dictionary value with a key 'foo' and a value 42 look like?

```python
{'foo': 42}
```
---

### 3. What is the main difference between a dictionary and a list?

* Lists are ordered while dictionary are unordered

---

### 4. What happens if you try to access spam['foo'] if spam is {'bar': 100}?


 ```100``` is returned

---

### 5. If a dictionary is stored in spam, what is the difference between the expressions 'cat' in spam and 'cat' in spam.keys()?

Same

---

### 6. If a dictionary is stored in spam, what is the difference between the expressions 'cat' in spam and 'cat' in spam.values()?

```python
'cat' in spam # returns true if 'cat' exists as a key in spam
'cat' in spam.values() # returns true if 'cat' exists as a value in spam
```

---

### 7. What is a shortcut for the following code?
```python
if 'color' not in spam:
    spam['color'] = 'black'

spam.setdefault('color': black)
```

---

### 8. What module and function can be used to “pretty print” dictionary values?

pprint module pprint function