import os
import csv
import openpyxl

for excelFile in os.listdir('.'):
    # Skip non-xlsx files, load the workbook object.
    if not (excelFile.endswith(".xlsx")):
        continue

    wb = openpyxl.load_workbook(excelFile)
    
    for sheetName in wb.sheetnames:
        # Loop through every sheet in the workbook.
        sheet = wb[sheetName]

        # Create the CSV filename from the Excel filename and sheet title.
        output_file = open(f"{excelFile[:-5]}_{sheetName}.csv", "w")
        # Create the csv.writer object for this CSV file.
        output_writer = csv.writer(output_file)

        # Loop through every row in the sheet.
        for rowNum in range(1, sheet.max_row + 1):
            rowData = []    # append each cell to this list
            # Loop through each cell in the row.
            for colNum in range(1, sheet.max_column + 1):
                rowData.append(sheet.cell(row=rowNum, column=colNum).value)
                # Append each cell's data to rowData.

            # Write the rowData list to the CSV file.
            output_writer.writerow(rowData)
            
        output_file.close()