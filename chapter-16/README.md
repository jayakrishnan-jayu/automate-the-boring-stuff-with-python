### 1. What are some features Excel spreadsheets have that CSV spread-sheets don’t?

* have settings for font size or color
* can have multiple worksheets
* cell widths and heights can be specified
* merged cells
* Images or charts can be embedded in them

---

### 2. What do you pass to csv.reader() and csv.writer() to create reader and writer objects?

A file object

---

### 3. What modes do File objects for reader and writer objects need to be opened in?

* `'r'` for reader object
* `'w'` for writer object

---

### 4. What method takes a list argument and writes it to a CSV file?

`writerow()`

---

### 5. What do the delimiter and lineterminator keyword arguments do?

* `delimiter` argument changes the default character that appears between cells in a row.
* `lineterminator` argument changes the default character that comes at the end of the row.

---

### 6. What function takes a string of JSON data and returns a Python data structure?

`json.loads()`

---

### 7. What function takes a Python data structure and returns a string of JSON data?

`json.dumps()`