
### 1. What is the difference between shutil.copy() and shutil.copytree()?

* ```shutil.copy``` will copy a single file
* ```shutil.copytree``` will copy a folder and its contents.

---

### 2. What function is used to rename files?

```shutil.move()```

---

### 3. What is the difference between the delete functions in the send2trash and shutil modules?

* ```shtil`` module will delete the file permanantly.
* ```send2trash``` module will move the file to the recycle bin.

---

### 4. ZipFile objects have a close() method just like File objects’ close() method. What ZipFile method is equivalent to File objects’ open() method?

```zipfile.ZipFile()```