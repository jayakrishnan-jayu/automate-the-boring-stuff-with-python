import os
from pathlib import Path

path_to_folder = ""
while not Path(path_to_folder).is_absolute():
    path_to_folder = input("Enter absolute path: ")

for path, dirs, files in os.walk(path_to_folder):
    for filename in files:
        if (os.path.getsize(f"{path}/{filename}")) > 100 * 1024 * 1024:
            print("Deleting",filename)
            os.unlink(f"{path}/{filename}")