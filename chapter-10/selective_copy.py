from pathlib import Path
import shutil, glob

path_to_folder = ""
while not Path(path_to_folder).is_absolute():
    path_to_folder = input("Enter absolute path: ")


for x in glob.glob(str(Path(path_to_folder))+"/**",recursive = True): 
    if x.endswith(".py"):
        print(x)
        shutil.copy(x, Path.home())
