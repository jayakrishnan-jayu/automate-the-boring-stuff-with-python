### 1. What are the two values of the Boolean data type? How do you write them?

* Two values of boolean data type are true and false.
* They are written as ```True``` and ```False```

---

### 2. What are the three Boolean operators?

```python
and
or
not
``` 

---

### 3. Write out the truth tables of each Boolean operator (that is, every possible combination of Boolean values for the operator and what they evaluate to).

#### and operator

|x|y|x and y|
|-|-|-|
|0|0|0|
|0|1|0|
|1|0|0|
|1|1|1|

#### or operator

|x|y|x or y|
|-|-|-|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|1|

#### not operator

|x|not x|
|-|-|
|0|1|
|1|0|

---

### 4. What do the following expressions evaluate to?

```python
>>> (5 > 4) and (3 == 5) # False
>>> not (5 > 4) # False
>>> (5 > 4) or (3 == 5) # True
>>> not ((5 > 4) or (3 == 5)) # False
>>> (True and True) and (True == False) # False
>>> (not False) or (not True) # True
```

---

### 5. What are the six comparison operators?

* ```>``` greater than
* ```>=``` greater than or equal to
* ```<``` lesser than
* ```<=``` lesser than or equal to
* ```==``` is equal to
* ```!=``` not eqal to

---

### 6. What is the difference between the equal to operator and the assignment operator?

* equal to operator will return a boolean value after checking if the value on the right side is equal to that of left side.
* assignment operator will asign the value from right to left

---

### 7. Explain what a condition is and where you would use one.

* conditions are expressions that will evaluate to a boolean value.
* they are used in control flow statements.

---

### 8. Identify the three blocks in this code:

```python
spam = 0                
if spam == 10:
##################################### Block 1
    print('eggs')                   # 
    if spam > 5:                    #
        ############### Block 2     #         
        print('bacon')#             #
        ###############             #
    else:                           #
        ############### Block 3     #
        print('ham')  #             #
        ###############             #
    print('spam')                   #
#####################################
print('spam')  
```

---

### 9. Write code that prints Hello if 1 is stored in spam, prints Howdy if 2 is stored in spam, and prints Greetings! if anything else is stored in spam.

```python
spam = 1
if (spam == 1):
    print("Hello")
elif (spam == 2):
    print("Howdy")
else:
    print("Greetings!")
```

---

### 10. What keys can you press if your program is stuck in an infinite loop?

* ```ctrl + c```

---

### 11. What is the difference between break and continue?

|break|continue|
|-|-|
|Used to exit from loop|Used to skip current iteration of the loop|

---

### 12. What is the difference between range(10), range(0, 10), and range(0, 10, 1) in a for loop?

* The output of the three is the same.

---

### 13. Write a short program that prints the numbers 1 to 10 using a for loop. Then write an equivalent program that prints the numbers 1 to 10 using a while loop.

```python
for i in range(1, 11):
    print(i)

i = 0
while (i<=10):
    print(i)
```

---

### 14. If you had a function named bacon() inside a module named spam, how would you call it after importing spam?

```python
spam.bacon()
```