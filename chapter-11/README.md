### 1. Write an assert statement that triggers an AssertionError if the variable spam is an integer less than 10.

```python
assert spam >= 10, f"spam is {spam}"
```

---

### 2. Write an assert statement that triggers an AssertionError if the variables eggs and bacon contain strings that are the same as each other, even if their cases are different (that is, 'hello' and 'hello' are considered the same, and 'goodbye' and 'GOODbye' are also considered the same).

```python
assert eggs.lower() != bacon.lower(), "Bacon and eggs are the same"
```

---

### 3. Write an assert statement that always triggers an AssertionError.

assert False

---

### 4. What are the two lines that your program must have in order to be able to call logging.debug()?

```python
import logging
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s')
```

---

### 5. What are the two lines that your program must have in order to have logging.debug() send a logging message to a file named programLog.txt?

```python
import logging
logging.basicConfig(filename='programLog.txt' level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s')
```


---

### 6. What are the five logging levels?

* ```DEBUG```
* ```INFO```
* ```WARNING```
* ```ERROR```
* ```CRITICAL```

---

### 7. What line of code can you add to disable all logging messages in your program?

```python
logging.disable(logging.CRITICAL)
```

---

### 8. Why is using logging messages better than using print() to display the same message?

* It allows the programmer to switch of logging by edditing a single line rather than commenting out the print statement or deleting them.

---

### 9. What are the differences between the Step Over, Step In, and Step Out buttons in the debugger?

* Step over wil execute function call and stops after return.
* Step in will cause the debugger to execute the next line of code and pause.
* step out will exectute the the remaing code in the function and stops after return.

---

### 10. After you click Continue, when will the debugger stop?

* The program will be executed normally until it terminates or reaches a breakpoint.

---

### 11. What is a breakpoint?

* Breakpoint can be set on a line of code that enables the debugger to puase at that line.

---

### 12. How do you set a breakpoint on a line of code in Mu?

* Click the line number.

