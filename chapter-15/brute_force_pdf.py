import PyPDF2

with open("dictionary.txt", "r") as myFile:
    
    pdf_reader = PyPDF2.PdfFileReader(open("encrypted.pdf", "rb"))
    pdf_writer = PyPDF2.PdfFileWriter()

    for word in myFile.read().split("\n"):
          
        if pdf_reader.decrypt(word):
            print("Decrypted, Password is "+ word)
            break
    print("Password not found")