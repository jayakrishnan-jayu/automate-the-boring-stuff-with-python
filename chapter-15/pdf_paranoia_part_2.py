import os
import sys
import PyPDF2
from pathlib import Path

from pdf_paranoia import decrypt_pdf, write_pdf

def main():
    if not len(sys.argv) == 2:
        print("syntax: pdf_paranoia_part_2.py password")
        return
    
    pswd = sys.argv[1]
    for root, dirs, files in os.walk("."):
        for fileName in files:
            if fileName.endswith(".pdf"):
                decrypted_pdf_writer = decrypt_pdf(Path(os.path.join(root, fileName)), pswd)
                
                if decrypted_pdf_writer:
                    output_path = os.path.join(root, fileName.replace("_encrypted", ""))
                    write_pdf(decrypted_pdf_writer, output_path)
                    print("Done decrypting")
                else:
                    print("The password is incorrect or the file is already decrypted")

if __name__ == "__main__":
    main()