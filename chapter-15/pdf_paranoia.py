import os
import sys
import PyPDF2
from pathlib import Path

def encrypt_pdf(file_path, pswd):
    pdf_reader = PyPDF2.PdfFileReader(open(str(file_path.absolute()), 'rb'))
    pdf_writer = PyPDF2.PdfFileWriter()

    if not pdf_reader.isEncrypted:
        pdf_writer = PyPDF2.PdfFileWriter()
        
        for page_num in range(pdf_reader.numPages):
            pdf_writer.addPage(pdf_reader.getPage(page_num))
        
        pdf_writer.encrypt(pswd)

        return pdf_writer
        
    else:
        print(f"{file_path.absolute()} is already encrypted")
        return None


def decrypt_pdf(file_path, pswd):
    
    pdf_reader = PyPDF2.PdfFileReader(open(str(file_path.absolute()), "rb"))
    pdf_writer = PyPDF2.PdfFileWriter()

    if pdf_reader.isEncrypted and pdf_reader.decrypt(pswd):

        try:
            for page_num in range(pdf_reader.numPages):
                pdf_writer.addPage(pdf_reader.getPage(page_num))
        except Exception:
            return None

        return pdf_writer

    return None

def write_pdf(pdf_writer, output_filename):
    result_pdf = open(output_filename, "wb")
    pdf_writer.write(result_pdf)
    result_pdf.close()


def main():
    if not len(sys.argv) == 2:
        print("syntax: pdf_paranoia.py password")
        return
    
    pswd = sys.argv[1]
    for root, dirs, files in os.walk("."):
        for fileName in files:
            if fileName.endswith(".pdf"):

                file_path = Path(os.path.join(root, fileName))
                encrypted_pdf_writer = encrypt_pdf(file_path, pswd)

                if encrypted_pdf_writer:
                    output_filename = f"{file_path.parent}/{file_path.stem}_encrypted.pdf"
                    
                    write_pdf(encrypted_pdf_writer, output_filename)

                    decrypted_pdf_writer = decrypt_pdf(Path(output_filename), pswd)

                    if not decrypted_pdf_writer:
                        print("Error occured decrypting file " + output_filename)
                        return

                    print("Succesfully decrypted" + output_filename)
                    print("deleting..")
                    os.unlink(str(file_path.absolute()))


if __name__ == "__main__":
    main()