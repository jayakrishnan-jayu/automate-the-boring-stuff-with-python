### 1. Why are functions advantageous to have in your programs?

* Functions can be executed multiple types by calling it.
* It will improve the readablity and modularity of the code.
---

### 2. When does the code in a function execute: when the function is defined or when the function is called?

* function will be executed when it is called.

---

### 3. What statement creates a function?

```def```

---

### 4. What is the difference between a function and a function call?

* A function is a set of statements.
* A function call is a function's name followed by parantheses.

---

### 5. How many global scopes are there in a Python program? How many local scopes?

* There is only one global scope and one local scope

---

### 6. What happens to variables in a local scope when the function call returns?

* The local scope gets destroyed.

---

### 7. What is a return value? Can a return value be part of an expression?

* return value is what a function call evaluates to.
* return value can be a part of expression as it returns a value

---

### 8. If a function does not have a return statement, what is the return value of a call to that function?

None

---

### 9. How can you force a variable in a function to refer to the global variable?

using the keyword ```global``` before the local variable

---

### 10. What is the data type of None?

NoneType

---

### 11. What does the import areallyourpetsnamederic statement do?

* It will import the code from areallyourpetsnamederic.py

---

### 12. If you had a function named bacon() in a module named spam, how would you call it after importing spam?

```python
spam.bacon()
```

---

### 13. How can you prevent a program from crashing when it gets an error?

* By using try excpet statement

---

### 14. What goes in the try clause? What goes in the except clause?

* statements that might raise an error will go in try clause
* code that should be exectued when an error is thrown should be inside except clause.