def collatz(number):

    if (number % 2 == 0):

        print(number//2)
        return number//2 
    
    print(3 * number + 1)
    return 3 * number + 1


try:
    n = int(input("Enter number "))
    while (n>1):
        n = collatz(n)

except ValueError:
    print("Please enter an integer")