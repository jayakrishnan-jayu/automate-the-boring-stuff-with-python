### 1. What is a relative path relative to?

* It is relative to the present working directory.

---

### 2. What does an absolute path start with?

* Absolute path starts with the root folder.

---

### 3. What does Path('C:/Users') / 'Al' evaluate to on Windows?

* It evaluates to a WindowsPath object with a path of 'C:/Users/Al'

---

### 4. What does 'C:/Users' / 'Al' evaluate to on Windows?

* It will throw an error becuase the starting operand is a string not a path object

---

### 5. What do the os.getcwd() and os.chdir() functions do?

* ```os.getcwd()``` will return the current working directory.
* ```os.chdir()``` will change the current working directory to the passed value.
---

### 6. What are the . and .. folders?

* ```.``` is the present working folder and ```..``` is the parent folder.

---

### 7. In C:\bacon\eggs\spam.txt, which part is the dir name, and which part is the base name?

* ```C:\bacon\eggs``` is the dir name.
* ```spam.txt``` is the base name.

---

### 8. What are the three “mode” arguments that can be passed to the open() function?

* ```'r'``` Read mode
* ```'w'``` Write mode
* ```'a'``` Append mode

---

### 9. What happens if an existing file is opened in write mode?

* The existing content is erased.

---

### 10. What is the difference between the read() and readlines() methods?

* ```read()``` will return a string of the content in the file while ```readlines``` will return a list of every line in the file.

---

### 11. What data structure does a shelf value resemble?

* dictionary