import re

with open ("text.txt") as myFile:
    a = myFile.read()
    regex = re.compile("(ADJECTIVE)|(NOUN)|(ADVERB)|(VERB)")

    for i in regex.findall(a):
        for j in i:
            if j != '':
                part_of_speech = re.compile(j)
                print(part_of_speech)
                if j.startswith("AD"):
                    print("Enter an ",j.lower())
                else:
                    print("Enter a ",j.lower())
                a = part_of_speech.sub(input(), a, 1)

    print(a)