import pyinputplus as pyip


bread = {"wheat": 10, "white": 12, "surdough": 11}
protein = {"chicken": 25, "turkey": 27, "ham": 26, "tofu": 22}

cheese = {"cheddar": 7, "swiss": 8, "mozzarella": 10}
specials = {"mayo": 5, "mustard": 6, "lettuce": 4, "tomato": 6}



def ask():
    price = 0
    price += bread[pyip.inputMenu(list(bread.keys()))]
    price += protein[pyip.inputMenu(list(protein.keys()))]
    if pyip.inputYesNo("Do yo want cheese: ") == "yes":
         price += cheese[pyip.inputMenu(list(cheese.keys()))]
    for special in specials:
        if pyip.inputYesNo(f"Do you wante {special} ") == "yes":
            price += specials[special]
    
    print(f"Your price is {pyip.inputInt('How many sandwhiches do you want ', min=1) * price}")

ask()