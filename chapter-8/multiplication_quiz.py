import pyinputplus as pyip
from random import randint
from time import sleep

for _ in range(10):
    x,y = randint(0,9), randint(0,9)
    ans = x*y

    try:
        if ans == pyip.inputInt(f"What is the {x}X{y} ", timeout= 3):
            print("Correct!")
            sleep(1)
            continue
        print("Incorrect")
    except:
        print("You took longer than 8 seconds")
        continue
