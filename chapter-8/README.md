### 1. Does PyInputPlus come with the Python Standard Library?

No

---

### 2. Why is PyInputPlus commonly imported with import pyinputplus as pyip?

pyip is shorter than PyInputPlus which is easier to type.

---

### 3. What is the difference between inputInt() and inputFloat()?

* ```inputInt()``` return a integer value.
* ```inputFloat()``` return a float value.

---

### 4. How can you ensure that the user enters a whole number between 0 and 99 using PyInputPlus?

pass ```min=0, max=99``` as arguments to the respective input method.

---

### 5. What is passed to the allowRegexes and blockRegexes keyword arguments?

* allowRgexes argument specifies the regex pattern that is allowed.
* blockRgexes argument specifies the regex pattern that is blocked.

---

### 6. What does inputStr(limit=3) do if blank input is entered three times?

If blank input is entered three times, it will raise an RetryLimitException.

---

### 7. What does inputStr(limit=3, default='hello') do if blank input is entered three times?

If a input that is not a string is entered three times then 'hello' will be printed.