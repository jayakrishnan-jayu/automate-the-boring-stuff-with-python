import random
import smtplib
from time import sleep
smtpobj = smtplib.SMTP('smtp.gmail.com', 587)

smtpobj.ehlo()

smtpobj.starttls()

smtpobj.login("chore@chore.com", "password")


CHORES = ('dishes', 'bathroom', 'vacuum', 'walk dog')

users = {"user_1": {"email":"test@test1.com", "chore": None},
"user_2": {"email":"test@test2.com", "chore": None}, 
"user_3": {"email":"test@test3.com", "chore": None},
"user_4": {"email":"test@test4.com", "chore": None}}

def assign_chores():
    chores = list(CHORES)
    for user in users.keys():
        randomChore = ""
        while 1:
            randomChore = random.choice(chores)
            if users[user]["chore"] != randomChore:
                users[user]["chore"] = randomChore
                break

        chores.remove(randomChore)

def main():
    while 1:
        assign_chores()
        for user in users.keys():
            smtpobj.sendmail("chore@chore.com",users[user]["email"] ,  'Subject: Chores.\nThis weeks chore is '+ users[user]["chore"])
        sleep(7 * 24 * 60 * 60)
    

main()