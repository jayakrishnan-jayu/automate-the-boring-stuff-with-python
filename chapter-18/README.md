### 1. What is the protocol for sending email? For checking and receiving email?

* SMTP for sending email
* IMAP for checking and receving email

---

### 2. What four smtplib functions/methods must you call to log in to an SMTP server?

```python

smtpobj = smtplib.SMTP("smtp.gmail.com", 587)

smtpobj.ehlo()

smtpobj.starttls()

smtpobj.login("email@email.com", "passwrod")

```

---

### 3. What two imapclient functions/methods must you call to log in to an IMAP server?

```python
imapObj = imapclient.IMAPClient("imap.gmail.com", ssl=True)
mapObj.login("email@email.com", "password")
```

---

### 4. What kind of argument do you pass to imapObj.search()?

IMAP serach keys.

---

### 5. What do you do if your code gets an error message that says got more than 10000 bytes?

Disconect and reconnect to the IMAP server

---

### 6. The imapclient module handles connecting to an IMAP server and finding emails. What is one module that handles reading the emails that imapclient collects?

`pyzmail`

---

### 7. When using the Gmail API, what are the credentials.json and token.json files?

* credentials.json contains the client id and client secret which identifies the particular email address
* token.json is generated when the user authenticates the Gmail. If it is not present the user will be prompted to authenticate.

---

### 8. In the Gmail API, what’s the difference between “thread” and “message” objects?

gmail message is a single email, while a thread is a list of emails between each other.

---

### 9. Using ezgmail.search(), how can you find emails that have file attachments?

Pass `'has:attachment'` to search method.

---

### 10. What three pieces of information do you need from Twilio before you can send text messages?

* Account SID number
* Account authentication token
* Twilio number