### 1. What are escape characters?

* Escape characters consist of backslash + character
* They are used for including characters in string that otherwise might be impossible to do so.
---

### 2. What do the \n and \t escape characters represent?

* ```\n # Newline character```
* ```\t # Tab space```

---

### 3. How can you put a \ backslash character in a string?

```\\```

---

### 4. The string value "Howl's Moving Castle" is a valid string. Why isn’t it a problem that the single quote character in the word Howl's isn’t escaped?

* Becuase the string starts with double quotes.

---

### 5. If you don’t want to put \n in your string, how can you write a string with newlines in it?

* By using a multiline string

---

### 6. What do the following expressions evaluate to?

* ```'Hello, world!'[1] # outputs 'e'```
* ```'Hello, world!'[0:5] # outputs 'Hello'```
* ```'Hello, world!'[:5] # outputs 'Hello'```
* ```'Hello, world!'[3:] # outputs 'lo, world!'```


---

### 7. What do the following expressions evaluate to?

* ```'Hello'.upper() # outputs 'HELLO'```
* ```'Hello'.upper().isupper() # outputs True```
* ```'Hello'.upper().lower()# outputs 'hello'```

---

### 8. What do the following expressions evaluate to?

* ```'Remember, remember, the fifth of November.'.split() # output: ['Remember,', 'remember,', 'the', 'fifth', 'of', 'November.']```
* ```'-'.join('There can be only one.'.split()) # outptut: 'There-can-be-only-one.'```

---

### 9. What string methods can you use to right-justify, left-justify, and center a string?

* ```rjust() # right justify```
* ```ljust() # left justify```
* ```center() # center justify```

---

### 10. How can you trim whitespace characters from the beginning or end of a string?

* ```str.rstrip() # trip whitespace from right```
* ```str.lstrip() # trip whitespace from left```