tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]

def printTable(tableData):
    coldWidths = [0] * len(tableData)
    for index, column in enumerate(tableData):
        coldWidths[index] = max([len(x) for x in column])

    for x in range(len(tableData[0])):
        for y in range(len(tableData)):
            print(tableData[y][x].rjust(coldWidths[y]), end=" ")
        print()


printTable(tableData)