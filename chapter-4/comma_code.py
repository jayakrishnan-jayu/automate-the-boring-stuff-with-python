def comma_code(foo):
    if len(foo) == 0:
        return ""
    
    if len(foo) == 1:
        return str(foo[0])

    if len(foo) == 2:
        return str(foo[0]) + " and " + str(foo[1])

    output = ""
    
    for elem in foo[:-2]:
        output += str(elem) + ", "

    output += str(foo[-2]) + " and " + str(foo[-1])

    return output


print(comma_code([1, 4, 5, 6, 7, 3]))