### 1. What is []?

* Empty list

---

### 2. How would you assign the value 'hello' as the third value in a list stored in a variable named spam? (Assume spam contains [2, 4, 6, 8, 10].) 

```python
spam[2] = 'hello'
```
---

### For the following three questions, let’s say spam contains the list ['a', 'b', 'c', 'd'].


### 3. What does spam[int(int('3' * 2) // 11)] evaluate to?

'd'

---

### 4. What does spam[-1] evaluate to?

'd'

---

### 5. What does spam[:2] evaluate to? 

```python
['a', 'b']
```

---

### For the following three questions, let’s say bacon contains the list [3.14, 'cat', 11, 'cat', True].

### 6. What does bacon.index('cat') evaluate to?

1

---

### 7. What does bacon.append(99) make the list value in bacon look like?

```python
[3.14, 'cat', 11, 'cat', True, 99]
```

---

### 8. What does bacon.remove('cat') make the list value in bacon look like?

```python
[3.14, 11, 'cat', True]
```


---

### 9. What are the operators for list concatenation and list replication?

* List concatenation: +
* List replication: *

---

### 10. What is the difference between the append() and insert() list methods?

* append method will insert the value passed to the end of the list.
* insert method will take 2 parameters (index, value) and inserts the value at index of the list.

---

### 11. What are two ways to remove values from a list?
 
```python
list.remove(element)
del list[index]
```
---

### 12. Name a few ways that list values are similar to string values.

|string|list|
|-|-|
|starts with ```'``` or ```"``` and end with same | starts with ```[``` and ends with ```]```|
|```""``` is a empty string| ```[]``` is a empty list|
|characters of string can be accessed through index| elements of list can be accessed through index|

---

### 13. What is the difference between lists and tuples?

* Lists are mutable
* tuples are immutable

---

### 14. How do you type the tuple value that has just the integer value 42 in it?

```python
(42,)
```

---

### 15. How can you get the tuple form of a list value? How can you get the list form of a tuple value?

```python
tuple(myList) # tuple form of a list
list(myTuple) # list form of a tuple
```

---

### 16. Variables that “contain” list values don’t actually contain lists directly. What do they contain instead?

* They contain references to the list

---

### 17. What is the difference between copy.copy() and copy.deepcopy()?

* ```copy.copy()``` is used for creating a copy of a list
* ```copy.deepcopy()``` is used for createing a copy of a list if that list contains nested lists.