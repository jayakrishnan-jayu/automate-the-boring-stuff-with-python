import random

def generateRandomList():
    return [random.randint(0,1) for _ in range(100)]

def countStreak(foo):
    count = 0
    adj_count = 0
    streak = False
    for i in range(len(foo)-1):
        
        if foo[i] == foo[i+1]:
            if (streak):
                streak = False
                continue
            adj_count += 1
            

        else:
            streak = False
            adj_count = 0

        if adj_count == 5:
            count+=1
            adj_count = 0
            streak = True

    return count


streaks_count = 0
sample_size = 10000
for i in range(sample_size):

    streaks_count += countStreak(generateRandomList())

print("Chance of streak: {} %".format(round(streaks_count/sample_size, 2)))