import sys
from os import path

import openpyxl

def main():

    if len(sys.argv) != 2:
        print("Invalid syntax: format is \ncell_converter.py file_name")
        return
    
    if len(sys.argv) == 2 and not (path.exists(sys.argv[1]) and sys.argv[1].endswith(".xlsx")):
        print("File not found or invalid file format")
        return

    input_wb = openpyxl.load_workbook(sys.argv[1])
    input_sheet = input_wb.active

    output_wb = openpyxl.Workbook()
    output_sheet = output_wb.active

    
    for i in range(1, max(input_sheet.max_row, input_sheet.max_column)+1):
        for j in range(1, min(input_sheet.max_row, input_sheet.max_column)+1):
            output_sheet.cell(row=j, column=i).value = input_sheet.cell(row=i, column=j).value
            
    
    output_wb.save("inverted.xlsx")

main()