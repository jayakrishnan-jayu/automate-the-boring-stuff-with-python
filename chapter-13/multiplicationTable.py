import sys
import openpyxl

def main():

    if not (len(sys.argv) == 2 and sys.argv[1].isdigit()):
        print("Invalid syntax: format is multiplicationTable.py number")
        return

    wb = openpyxl.Workbook()
    sheet = wb.active

    n = int(sys.argv[1])

    fontObj = openpyxl.styles.Font(bold = True)

    for index in range(2, n+2):
        r_cell = sheet.cell(row=index, column=1)
        c_cell = sheet.cell(row=1, column=index)
        r_cell.font = fontObj
        c_cell.font = fontObj
        r_cell.value = index - 1
        c_cell.value = index - 1

    for row_index in range(2, n+2):
        for col_index in range(2, n+2):
            sheet.cell(row=row_index, column=col_index).value = (row_index-1) * (col_index-1)

    wb.save("multiplication.xlsx")

main()