import sys
from os import path

import openpyxl

def main():

    if not (len(sys.argv) == 4 and sys.argv[1].isdigit() and sys.argv[2].isdigit()):
        print("Invalid syntax: format is \nblankRowInserter.py m n file_name")
        return

    if not (path.exists(sys.argv[3]) and sys.argv[3].endswith(".xlsx")):
        print("File not found or invalid file format")
        return
    
    x, y, file_name = sys.argv[1:4]

    wb = openpyxl.load_workbook(file_name)
    sheet = wb.active
    sheet.insert_rows(int(x),int(y))

    
    wb.save(file_name)

main()