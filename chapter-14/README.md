### 1. What three files do you need for EZSheets to access Google Sheets?

* credentials-sheets.json
* token-sheets.pickle
* token-drive.pickle   

---

### 2. What two types of objects does EZSheets have?

* ezsheet.Sheet
* ezsheet.Spreadsheet

---

### 3. How can you create an Excel file from a Google Sheet spreadsheet?

By using `downloadAsExcel()` method

---

### 4. How can you create a Google Sheet spreadsheet from an Excel file?

By using `ezsheets.upload()` method

---

### 5. The ss variable contains a Spreadsheet object. What code will read data from the cell B2 in a sheet titled “Students”?

`ss['Students']['B2']`

---

### 6. How can you find the column letters for column 999?

`ezsheets.getColumnLetterOf(999)`

---

### 7. How can you find out how many rows and columns a sheet has?

By using `sheet.rowCount` and `sheet.columnCount`

---

### 8. How do you delete a spreadsheet? Is this deletion permanent?

By using `shee.delete()`. Yes deletig is permanent

---

### 9. What functions will create a new Spreadsheet object and a new Sheet object, respectively?

`ezsheets.createSpreadsheet()` and `ezsheet.createSheet()`

---

### 10. What will happen if, by making frequent read and write requests with EZSheets, you exceed your Google account’s quota?

if the quota is exceeded `googleapiclient.errors.HttpError` exception is raised but it catched by ezsheets and retry the request.