import ezsheets
ss = ezsheets.Spreadsheet('id_here')

sheet = ss[0]

rows = sheet.getRows()

for index, row in enumerate(rows):
    if index == 0:
        continue
    if row[0] == "":
        break
    x, y, z = [int(x) for x in row[:3]]
    if not x*y == z:
        print(f"Error found at index {index+1}")
        print(row)
