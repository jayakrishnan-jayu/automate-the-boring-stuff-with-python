### 1. What is the Unix epoch?

The unix epoch is a standard time reference (Jan 1 at midnight, 1970, UTC)for programming languages.

---

### 2. What function returns the number of seconds since the Unix epoch?

`time.time()`

---

### 3. How can you pause your program for exactly 5 seconds?

` time.sleep()`

---

### 4. What does the round() function return?

An integer or a float depending upon the paramters

---

### 5. What is the difference between a datetime object and a timedelta object?

* `datetime` object stores a particular time.
* `timedelta` object stores a duration of time.

---

### 6. Using the datetime module, what day of the week was January 7, 2019?

```python
datetime.datetime(2019, 1, 7).weekday() # Retrurns 0 which is Monday
```

---

### 7. Say you have a function named spam(). How can you call this function and run the code inside it in a separate thread?

```python
threadObj = threading.Thread(target=spam)
threadObj.start()
```

---

### 8. What should you do to avoid concurrency issues with multiple threads?

* Do not use variable common to different threads.